import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {Crearpersonarequestmodel} from "../interfaces/crearpersonarequestmodel.interface";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl: string = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  guardarPersona(persona: Crearpersonarequestmodel): Observable<number> {
    return this.httpClient.post<number>(this.baseUrl + '/personas/', persona);
  }
}
