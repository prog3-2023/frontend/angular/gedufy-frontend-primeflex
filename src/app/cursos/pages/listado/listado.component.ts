import {Component, OnInit} from '@angular/core';
import {CursosService} from "../../services/cursos.service";
import {Curso} from "../../interfaces/curso.interface";

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {

  cursos: Curso[] = [];

  constructor(private cursosService: CursosService) {
  }

  ngOnInit(): void {
    this.cursosService.getCursos().subscribe(resp => {
      this.cursos = resp;
    });
  }

}
